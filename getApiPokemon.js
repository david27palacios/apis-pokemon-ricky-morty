console.log(" *** Get Api ***");

const url = "https://pokeapi.co/api/v2/pokemon/?offset=00&limit=10";
const btnPrev = document.getElementById("btnPrev");
const btnnext = document.getElementById("btnnext");


// obtener los datos de la api

const getData = (api, opc) => {

    return fetch(api)
        .then((response) => response.json())
        .then((json) => {
            if(opc == 0)
            obtenerUrl(json);
            else
             imprimirDatos(json);
        })
        .catch ((error) => {
            console.log("Error... caray ...", error)
        })

};

let todaData;
let html;

const obtenerUrl = (data) => {

    todaData=data;

    data.results.forEach(pokemon => {
        html="";
        getData(pokemon.url, 1);
    });


}

// imprimir los resulados 
const  imprimirDatos = (data) => {
    existe=validarPaginacion(todaData);
    console.log(existe);
         // pintar los matachivos 
    html += `<div class="cards">`;
    html += `<div>`;
    html += `<img class="formatoPic" src="${data.sprites.other.dream_world.front_default}">`;
    html += `</div>`;
    html += `<div class="contenedorTextos">`;
    html += `<small class="txtLabel"> Nombre </Small>`;
    html += `<p class="txtTexto">${data.name}</p>`;
    data.abilities.forEach(habilidad => {
        html += `<p class="txtTexto">${habilidad.ability.name}</p>`; 
    });
    html += `</div>`;
    html += `</div>`; 

        //console.log ( "Nombre [ " + matachito.name + " ] Especie [ " + matachito.species + " ] pic [ " +  matachito.image +" ]")
        document.getElementById("contendedorTodo").innerHTML = html
    
    if(existe==1){
    const btnPrev = document.getElementById("btnPrev");
    btnPrev.style.display = "none";
    const alerta1=document.getElementById("alerta1");
    alerta1.innerHTML='No hay registros previos :('
    }
    if(existe==0){
        const btnPrev = document.getElementById("btnPrev");
        btnPrev.style.display = "block";
        const btnNext = document.getElementById("btnnext");
        btnNext.style.display = "block";
        const alerta2=document.getElementById("alerta2");
    alerta1.innerHTML=''
    alerta2.innerHTML=''
    }
    if(existe==2){
        const btnNext = document.getElementById("btnnext");
        btnNext.style.display = "none";
       
        const alerta2=document.getElementById("alerta2");
    alerta2.innerHTML='No hay mas registros :('
    }
    
   
   
}

// validar la paginacion 

const validarPaginacion = (data) => {
    var validar=0;
    if (data.previous == null ) {
        console.log("no se puede echar pa'tras ")
        validar=1;
    } else {
        console.log("si existen paginas previas ")
    }

    if (data.next == null) {
        console.log ("no se puede echar pa' lante ");
        validar=2;
    } else {
        console.log ("exiten paginas siguientes")
    }
    return validar;
}

// paginacion o navegacion



btnPrev.addEventListener('click', () => {
    // navegar haciar atras  ...............enviar la url con info.prev
    
    getData(todaData.previous,0)
})

btnnext.addEventListener('click', () => {
   console.log(todaData)
    // navegar haciar adelante  ..................enviar la url con info.next
    getData(todaData.next,0)
})


// consumimos o invocamos la api

getData(url, 0);
console.log(todaData);