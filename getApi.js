console.log(" *** Get Api ***");

const url = "https://rickandmortyapi.com/api/character/";
const btnPrev = document.getElementById("btnPrev");
const btnnext = document.getElementById("btnnext");


// obtener los datos de la api

const getData = (api) => {
    

    return fetch(api)
        .then((response) => response.json())
        .then((json) => {
             imprimirDatos(json)
        })
        .catch ((error) => {
            console.log("Error... caray ...", error)
        })

};

let todaData;
 
// imprimir los resulados 
const  imprimirDatos = (data) => {
    
    todaData=data;
    existe=validarPaginacion(todaData);
    console.log(existe);

    let = html = "";
  

    data.results.forEach(matachito => {
        

         // pintar los matachivos 
    html += `<div class="cards">`;
    html += `<div>`;
    html += `<img class="formatoPic" src="${matachito.image}">`;
    html += `</div>`;
    html += `<div class="contenedorTextos">`;
    html += `<small class="txtLabel"> Nombre </Small>`;
    html += `<p class="txtTexto">${matachito.name}</p>`;
    html += `<small class="txtLabel"> Especie </Small>`;
    html += `<p class="txtTexto">${matachito.species}</p>`;
    html += `</div>`;
    html += `</div>`; 

        //console.log ( "Nombre [ " + matachito.name + " ] Especie [ " + matachito.species + " ] pic [ " +  matachito.image +" ]")
        document.getElementById("contendedorTodo").innerHTML = html
    });

    if(existe==1){
    const btnPrev = document.getElementById("btnPrev");
    btnPrev.style.display = "none";
    const alerta1=document.getElementById("alerta1");
    alerta1.innerHTML='No hay registros previos :('
    }
    if(existe==0){
        const btnPrev = document.getElementById("btnPrev");
        btnPrev.style.display = "block";
        const btnNext = document.getElementById("btnnext");
        btnNext.style.display = "block";
        const alerta2=document.getElementById("alerta2");
    alerta1.innerHTML=''
    alerta2.innerHTML=''
    }
    if(existe==2){
        const btnNext = document.getElementById("btnnext");
        btnNext.style.display = "none";
       
        const alerta2=document.getElementById("alerta2");
    alerta2.innerHTML='No hay mas registros :('
    }
    
   
   
}

// validar la paginacion 

const validarPaginacion = (data) => {
    var validar=0;
    if (data.info.prev == null ) {
        console.log("no se puede echar pa'tras ")
        validar=1;
    } else {
        console.log("si existen paginas previas ")
    }

    if (data.info.next == null) {
        console.log ("no se puede echar pa' lante ");
        validar=2;
    } else {
        console.log ("exiten paginas siguientes")
    }
    return validar;
}

// paginacion o navegacion



btnPrev.addEventListener('click', () => {
    // navegar haciar atras  ...............enviar la url con info.prev
    console.log(todaData.info.prev)
    getData(todaData.info.prev)
})

btnnext.addEventListener('click', () => {
   
    // navegar haciar adelante  ..................enviar la url con info.next
    getData(todaData.info.next)
})


// consumimos o invocamos la api

getData(url);

/*

// vamos a pintar n cantidad de cards 

let = html = "";
for (let i = 1; i <=21; i++) {
    // pintar la o las cajas
    html += `<div class="cards">`;
    html += `<div>`;
    html += `<img class="formatoPic" src="./ironMan.jpg">`;
    html += `</div>`;
    html += `<div class="contenedorTextos">`;
    html += `<small class="txtLabel"> Nombre </Small>`;
    html += `<p class="txtTexto">Tony Stark </p>`;
    html += `<small class="txtLabel"> Heroe </Small>`;
    html += `<p class="txtTexto">Iron Man </p>`;
    html += `</div>`;
    html += `</div>`;  
}
document.getElementById("contendedorTodo").innerHTML = html

*/